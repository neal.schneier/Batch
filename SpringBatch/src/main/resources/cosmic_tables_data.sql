drop table people;
drop table person;

CREATE TABLE `people` (
  `firstName` varchar(16) NOT NULL,
  `lastName` varchar(40) NOT NULL,
  `status` varchar(16) NOT NULL,
  PRIMARY KEY (`firstName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `person` (
  `firstName` varchar(16) NOT NULL,
  `lastName` varchar(40) NOT NULL,
  `status` varchar(16) NOT NULL,
  PRIMARY KEY (`firstName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

alter table `people` add `status` varchar(16);

commit;

use cosmic;

select * from people ;
select * from person ;
select COUNT(*) from people;
select COUNT(*) from person;

update people set status='' where firstName like 'neal%';

delete from person where firstName like 'neal%';
