package SpringBatch;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

/**
 * Hello world!
 *
 */
@SpringBootApplication
public class App 
{
    private static Logger logger = Logger.getLogger(App.class);

    public static void main( String[] args )
    {
        long startTime = new Date().getTime();
        logger.info("Start Time: " + startTime);
        SpringApplication.run(App.class, args);
        logger.info("End Time: " + (new Date().getTime() - startTime));
    }
}
