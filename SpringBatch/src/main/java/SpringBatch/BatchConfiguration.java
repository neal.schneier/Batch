package SpringBatch;

import SpringBatch.domain.Person;
import SpringBatch.processor.PersonItemProcessor;
import SpringBatch.reader.PersonRowMapper;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Configuration
@EnableBatchProcessing
@EnableAutoConfiguration
public class BatchConfiguration {

    private static Logger log = Logger.getLogger(BatchConfiguration.class);

    @Autowired
    private JobBuilderFactory jobBuilders;

    @Autowired
    private StepBuilderFactory stepBuilders;

    @Autowired
    private JobRepository jobRepository;

    @Bean
    public JobLauncher jobLauncher() throws Exception {
        SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
        jobLauncher.setJobRepository(jobRepository);
        jobLauncher.setTaskExecutor(taskExecutorAsync());
        JobParameter jobParameter = new JobParameter(new Random().nextInt() + "");
        Map<String, JobParameter> map = new HashMap<String, JobParameter>();
        map.put("id", jobParameter);
        JobParameters jobParameters = new JobParameters(map);
        jobLauncher.afterPropertiesSet();
        long startTime = new Date().getTime();
        log.info(startTime);
        jobLauncher.run(importUserJob(jobBuilders, step1()), jobParameters);
        return jobLauncher;
    }

    @Bean
    public ItemReader<Person> reader() throws Exception {
        JdbcCursorItemReader<Person> itemReader = new JdbcCursorItemReader<Person>();
        itemReader.setDataSource(getDataSource());
        itemReader.setSql("SELECT * from people");
        itemReader.setRowMapper(new PersonRowMapper());
        return itemReader;
    }

    @Bean
    public ItemProcessor<Person, Person> processor() {
        return new PersonItemProcessor();
    }

    @Bean
    public ItemWriter<Person> writer(DataSource dataSource) {
        JdbcBatchItemWriter<Person> writer = new JdbcBatchItemWriter<Person>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Person>());
        writer.setSql("INSERT INTO person (firstName, lastName) VALUES (:firstName, :lastName)");
        writer.setDataSource(dataSource);
        return writer;
    }

    public Job importUserJob(JobBuilderFactory jobs, Step s1) throws SQLException {
        return jobs.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .start(s1)
                .build();
    }

    public Step step1() throws Exception {
        return stepBuilders.get("step1")
                .<Person, Person>chunk(1)
                .reader(reader())
                .processor(processor())
                .writer(writer(getDataSource()))
                .taskExecutor(taskExecutorAsync())
                .build();
    }


    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }


    public TaskExecutor taskExecutorAsync() {
        SimpleAsyncTaskExecutor taskExecutor = new SimpleAsyncTaskExecutor();
        taskExecutor.setConcurrencyLimit(-1);
        return taskExecutor;
    }

    public TaskExecutor taskExecutorPool() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(8);
        taskExecutor.initialize();
        return taskExecutor;
    }

    @Bean
    public DataSource getDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://localhost:3306/cosmic");
        dataSource.setUsername("root");
        dataSource.setPassword("potatOEz");
        return dataSource;
    }
}