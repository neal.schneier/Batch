package SpringBatch.processor;

import SpringBatch.domain.Person;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;

import java.util.Date;
import java.util.UUID;


/**
 * Created by NSchneier on 9/22/2015.
 */
public class PersonItemProcessor implements ItemProcessor<Person, Person> {

    private static Logger logger = Logger.getLogger(PersonItemProcessor.class);
    @Override
    public Person process(Person person) throws Exception {
        final String firstName = person.getFirstName().toUpperCase() + UUID.randomUUID().toString().substring(0, 5);
        final String lastName = person.getLastName().toUpperCase();
        //Thread.sleep(100);
        logger.info(new Date().getTime() + " " + person.toString());
        return new Person(firstName, lastName);
    }
}
